import { format } from "date-fns/format";
import { sub } from "date-fns/sub";
import { createRestAPIClient } from "masto";

interface MalojaChartArtist {
  scrobbles: number;
  real_scrobbles: number;
  artist_id: number;
  artist: string;
  rank: number;
  associated_artists: string[];
}

interface MalojaResponse<T = unknown> {
  status: string;
  list: T[];
  pagination?: {
    page: number;
    perpage: number | null;
    next_page: string | null;
    prev_page: string | null;
  };
}

type RequiredProps = {
  malojaUrl: string;
  mastodon: Parameters<typeof createRestAPIClient>[0];
};

class Poster {
  private mastodonClient: ReturnType<typeof createRestAPIClient>;
  private malojaUrl: string;

  constructor(props: RequiredProps) {
    this.mastodonClient = createRestAPIClient(props.mastodon);
    this.malojaUrl = props.malojaUrl;
  }

  static getPunc = (i: number, artistLength: number) =>
    i !== artistLength - 1 ? "," : "";

  static getJoiner = (i: number, artistLength: number) =>
    i !== artistLength - 2 ? Poster.getPunc(i, artistLength) : ", and";

  // create a string of artists with playcounts
  static createArtistString = (topArtists: MalojaChartArtist[]): string =>
    topArtists.reduce(
      (acc, artist, i) =>
        `${acc} ${
          artist.artist
        } (${artist.scrobbles.toString()})${Poster.getJoiner(
          i,
          topArtists.length,
        )}`,
      "",
    );

  // pulls the top artists of user in the past period as an array of size limit
  private getTopArtists = async (
    limit: number,
  ): Promise<MalojaResponse<MalojaChartArtist>> => {
    const queryUrl = new URL("/apis/mlj_1/charts/artists", this.malojaUrl);
    queryUrl.searchParams.append(
      "since",
      format(sub(new Date(), { days: 7 }), "yyyy/MM/dd"),
    );
    queryUrl.searchParams.append("perpage", limit.toString());

    try {
      const response = await fetch(queryUrl);
      if (response.ok) {
        const data =
          (await response.json()) as MalojaResponse<MalojaChartArtist>;
        return data;
      }
    } catch (_e: unknown) {
      return { status: "error", list: [] };
    }
    return { status: "error", list: [] };
  };
  // sends a post comprised of 'text'
  private postStatus = (status: string) => {
    if (process.env.DEBUG) {
      console.info("Created Post!");
      console.info(status);
      return;
    }
    return this.mastodonClient.v1.statuses.create({
      status,
      visibility: "public",
      sensitive: true,
      spoilerText: "Weekly scrobble autopost",
    });
  };

  private setupPost = async (artistArray: MalojaChartArtist[]) => {
    const postString = `Top artists this week:${Poster.createArtistString(
      artistArray,
    )}

(via ${this.malojaUrl})`;

    if (postString.length <= 400) {
      try {
        await this.postStatus(postString);
        console.info(`Successfully Posted!
          ${postString}`);
      } catch (e) {
        console.warn(`Couldn't post ${postString}`);
        console.warn(e, null, 2);
      }
    } else if (artistArray.length > 1) {
      console.info(
        `Too long ${postString}

        Trying with ${(artistArray.length - 1).toString()} artists`,
      );
      await this.setupPost(artistArray.splice(0, artistArray.length - 1));
    } else {
      console.info(
        `The top artist, ${Poster.createArtistString(
          artistArray,
        )}, has too long a name`,
      );
    }
  };

  public run = async (numArtists: number | undefined): Promise<void> => {
    if (!this.malojaUrl) {
      console.error("A maloja url is required");
      process.exit(1);
    }

    if (!numArtists || +numArtists < 1) {
      console.error("Number of artists must be greater than 0");
      process.exit(1);
    }
    try {
      const topArtists = await this.getTopArtists(+numArtists);
      if (topArtists.status === "ok") {
        await this.setupPost(topArtists.list.slice(0, numArtists));
      } else {
        throw new Error(topArtists.status);
      }
    } catch (e) {
      console.error(e);
    }
  };
}

export async function postMostPlayedArtists(): Promise<void> {
  const Post = new Poster({
    malojaUrl: process.env.MALOJA_URL ?? "",
    mastodon: {
      accessToken: process.env.MASTODON_ACCESS_TOKEN ?? "",
      url: process.env.MASTODON_API_URL ?? "",
    },
  });

  await Post.run(6);
}
